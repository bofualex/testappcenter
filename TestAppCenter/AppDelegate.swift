//
//  AppDelegate.swift
//  TestAppCenter
//
//  Created by Alex Bofu on 21/02/2019.
//  Copyright © 2019 Alex Bofu. All rights reserved.
//

import UIKit
import AppCenter
import AppCenterAnalytics
import AppCenterCrashes

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        MSAppCenter.start("c128c5a0-508a-4a6d-99a1-f298fe5cdb3a", withServices:[MSAnalytics.self, MSCrashes.self])
        
        return true
    }
}

